import tensorflow as tf
a = tf.constant(6.4,name='constant_a')
b = tf.constant(5.0,name='constant_b')
c = tf.constant(4.4, name="constant_c")
d = tf.constant(3.4,name="constant_d")

square = tf.square(a,name='square_a')
power = tf.pow(b,c,name='power_of_a_b')

sqrt = tf.sqrt(d,name='sqrt_d')

final_sum = tf.add_n([sqrt,square,power],name="finalsum")

sess = tf.Session()

print("The session square: {0}".format(sess.run(square)))
print("The session power: {0}".format(sess.run(power)))
print("The session square root: {0}".format(sess.run(sqrt)))
print("The final sum: {0}".format(sess.run(final_sum)))

another_sum = tf.add_n([a,b,c,d,square],name="anoth_sum")

writer = tf.summary.FileWriter('m1_example',sess.graph)
writer.close()
sess.close()